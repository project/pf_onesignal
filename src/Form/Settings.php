<?php

namespace Drupal\pf_onesignal\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\push_framework\Form\Settings as FrameworkSettings;

/**
 * Configure Push Framework OneSignal settings.
 */
class Settings extends FrameworkSettings {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'pf_onesignal_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['pf_onesignal.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['onesignal_details'] = [
      '#type' => 'container',
      '#states' => [
        'visible' => [
          ':input[name="active"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['onesignal_details']['appid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('AppId'),
      '#default_value' => $this->pluginConfig->get('appid'),
    ];
    $form['onesignal_details']['authkey'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Auth key'),
      '#default_value' => $this->pluginConfig->get('authkey'),
    ];
    $form['onesignal_details']['mobilebundleid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Mobile bundle ID'),
      '#default_value' => $this->pluginConfig->get('mobilebundleid'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->pluginConfig
      ->set('appid', $form_state->getValue('appid'))
      ->set('authkey', $form_state->getValue('authkey'))
      ->set('mobilebundleid', $form_state->getValue('mobilebundleid'));
    parent::submitForm($form, $form_state);
  }

}
