<?php

namespace Drupal\pf_onesignal\Plugin\PushFrameworkChannel;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityMalformedException;
use Drupal\pf_onesignal\Entity\Device;
use Drupal\push_framework\ChannelBase;
use Drupal\user\UserInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Plugin implementation of the push framework channel.
 *
 * @ChannelPlugin(
 *   id = "onesignal",
 *   label = @Translation("OneSignal"),
 *   description = @Translation("Provides the OneSignal channel plugin.")
 * )
 */
class OneSignal extends ChannelBase {

  public const API_URL = 'https://onesignal.com/api/v1/notifications';
  public const MAX_ATTEMPTS = 3;

  /**
   * {@inheritdoc}
   */
  public function getConfigName(): string {
    return 'pf_onesignal.settings';
  }

  /**
   * {@inheritdoc}
   */
  public function applicable(UserInterface $user): bool {
    return $this->active && !empty($this->getDeviceIds($user));
  }

  /**
   * {@inheritdoc}
   */
  public function send(UserInterface $user, ContentEntityInterface $entity, array $content, int $attempt): string {
    $ids = [];
    /** @var \Drupal\pf_onesignal\Entity\Device $device */
    foreach (Device::loadMultiple($this->getDeviceIds($user)) as $device) {
      $ids[] = $device->get('onesignaluserid')->value;
    }
    if (empty($ids)) {
      return self::RESULT_STATUS_FAILED;
    }
    try {
      $url = $entity->toUrl('canonical', [
        'absolute' => TRUE,
        'https' => TRUE,
      ])->toString();
    }
    catch (EntityMalformedException $e) {
      $this->logger->warning($e->getMessage());
      $url = '';
    }
    $body = [
      'app_id' => $this->pluginConfig->get('appid'),
      'include_player_ids' => $ids,
      'headings' => [],
      'contents' => [],
      'data' => [
        'targetUrl' => $url,
      ],
    ];
    $firstLanguage = array_keys($content)[0];
    foreach ($content as $language => $parts) {
      $body['headings'][$language] = $parts['subject'];
      $body['contents'][$language] = $parts['body'];
    }
    // Make sure that english is included, because OneSignal API requires that.
    if (empty($body['headings']['en'])) {
      $body['headings']['en'] = $body['headings'][$firstLanguage];
      $body['contents']['en'] = $body['contents'][$firstLanguage];
    }
    $options = [
      'headers' => [
        'Authorization' => 'Basic ' . $this->pluginConfig->get('authkey'),
        'Content-Type' => 'application/json',
      ],
      'body' => json_encode($body),
    ];

    $client = new Client();
    try {
      $response = $client->request('POST', self::API_URL, $options);
      $statusCode = $response->getStatusCode();
      if ($statusCode === 200) {
        return self::RESULT_STATUS_SUCCESS;
      }
    }
    catch (GuzzleException $e) {
      $this->logger->error($e->getMessage());
    }

    return ($attempt < self::MAX_ATTEMPTS) ? self::RESULT_STATUS_RETRY : self::RESULT_STATUS_FAILED;
  }

  /**
   * Get the device IDs for the given user.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user.
   *
   * @return array
   *   The list of device IDs.
   */
  protected function getDeviceIds(UserInterface $user): array {
    try {
      return $this->entityTypeManager->getStorage('onesignal_device')
        ->getQuery()
        ->accessCheck(FALSE)
        ->condition('uid', $user->id())
        ->condition('status', 1)
        ->execute();
    }
    catch (InvalidPluginDefinitionException $e) {
      $this->logger->error($e->getMessage());
    }
    catch (PluginNotFoundException $e) {
      $this->logger->error($e->getMessage());
    }
    return [];
  }

}
