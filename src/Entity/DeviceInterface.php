<?php

namespace Drupal\pf_onesignal\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a device entity type.
 */
interface DeviceInterface extends ContentEntityInterface, EntityChangedInterface {

  /**
   * Returns the device status.
   *
   * @return bool
   *   TRUE if the device is enabled, FALSE otherwise.
   */
  public function isEnabled(): bool;

  /**
   * Sets the device status.
   *
   * @param bool $status
   *   TRUE to enable this device, FALSE to disable.
   *
   * @return \Drupal\pf_onesignal\Entity\DeviceInterface
   *   The called device entity.
   */
  public function setStatus($status): DeviceInterface;

}
