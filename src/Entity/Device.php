<?php

namespace Drupal\pf_onesignal\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

/**
 * Defines the device entity class.
 *
 * @ContentEntityType(
 *   id = "onesignal_device",
 *   label = @Translation("OneSignal Device"),
 *   label_collection = @Translation("OneSignal Devices"),
 *   handlers = {
 *     "views_data" = "Drupal\views\EntityViewsData"
 *   },
 *   base_table = "onesignal_device",
 *   admin_permission = "administer onesignal device",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "id",
 *     "uuid" = "uuid"
 *   },
 *   links = {},
 * )
 */
class Device extends ContentEntityBase implements DeviceInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public function isEnabled(): bool {
    return (bool) $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatus($status): DeviceInterface {
    $this->set('status', $status);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Status'))
      ->setDescription(t('A boolean indicating whether the device is enabled.'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Enabled');

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the device was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the device was last edited.'));

    $fields['appversion'] = BaseFieldDefinition::create('string')
      ->setLabel(t('App version'))
      ->setSetting('max_length', 255);

    $fields['os'] = BaseFieldDefinition::create('string')
      ->setLabel(t('OS'))
      ->setSetting('max_length', 255);

    $fields['language'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Language'))
      ->setSetting('max_length', 255);

    $fields['platform'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Platform'))
      ->setSetting('max_length', 255);

    $fields['osversion'] = BaseFieldDefinition::create('string')
      ->setLabel(t('OS Version'))
      ->setSetting('max_length', 255);

    $fields['model'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Model'))
      ->setSetting('max_length', 255);

    $fields['onesignaluserid'] = BaseFieldDefinition::create('string')
      ->setLabel(t('OneSignal user id'))
      ->setSetting('max_length', 255);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Owner'));

    return $fields;
  }

}
