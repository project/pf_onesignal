<?php

namespace Drupal\pf_onesignal\Controller;

use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Endpoint to receive OneSignal apple id validation.
 */
class AppleId extends ControllerBase {

  /**
   * The onesignal configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    $instance = parent::create($container);
    $instance->config = $container->get('config.factory')->get('pf_onesignal.settings');
    return $instance;
  }

  /**
   * Creates the JSON response for this controller.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response.
   */
  public function execute(): JsonResponse {
    return new JsonResponse([
      'applinks' => [
        'apps' => [],
        'details' => [
          [
            'appID' => $this->config->get('mobilebundleid'),
            'paths' => [
              '*',
            ],
          ],
        ],
      ],
    ]);
  }

}
