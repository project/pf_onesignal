<?php

namespace Drupal\pf_onesignal\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\pf_onesignal\Entity\Device;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Endpoint to receive OneSignal device registration.
 */
class Register extends ControllerBase {

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected Request $request;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->currentUser = $container->get('current_user');
    $instance->request = $container->get('request_stack')->getCurrentRequest();
    return $instance;
  }

  /**
   * Get the title of this controller.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The title.
   */
  public function title(): TranslatableMarkup {
    return $this->t('System endpoint');
  }

  /**
   * Check access to this controller for the current user.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(): AccessResultInterface {
    if ($this->currentUser()->isAuthenticated()) {
      return AccessResult::allowed();
    }
    return AccessResult::forbidden();
  }

  /**
   * Execute this controller and build response.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function execute(): Response {
    $body = $this->request->getContent();
    try {
      $info = json_decode($body, TRUE, 512, JSON_THROW_ON_ERROR);
    }
    catch (\JsonException $e) {
      $this->getLogger('onesignal')->warning('OneSignal fail registration due to bad json input: :message - :body', [
        ':message' => $e->getMessage(),
        ':body' => $body,
      ]);
      return new Response('Bad json input.');
    }
    if (!isset($info['oneSignalUserId'])) {
      return new Response('Bad request', 400);
    }
    $ids = $this->entityTypeManager()->getStorage('onesignal_device')
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('onesignaluserid', $info['oneSignalUserId'])
      ->condition('uid', $this->currentUser()->id())
      ->execute();
    if (empty($ids)) {
      $device = Device::create([
        'onesignaluserid' => $info['oneSignalUserId'],
        'uid' => $this->currentUser()->id(),
      ]);
    }
    else {
      $device = Device::load(reset($ids));
    }
    $fields = [
      'appversion' => 'appVersion',
      'os' => 'os',
      'language' => 'language',
      'platform' => 'platform',
      'osversion' => 'osVersion',
      'model' => 'model',
      'onesignaluserid' => 'oneSignalUserId',
    ];
    $changed = FALSE;
    foreach ($fields as $key => $value) {
      if (isset($info[$value]) && $device->get($key)->value !== $info[$value]) {
        $changed = TRUE;
        $device->set($key, $info[$value]);
      }
    }
    if ($changed) {
      $device->save();
    }
    return new Response('thx');
  }

}
