<?php

namespace Drupal\pf_onesignal;

use Drupal\Core\TempStore\PrivateTempStore;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\Core\TempStore\TempStoreException;

/**
 * Private temporary store for push framework onesignal access.
 */
class PrivateStore {

  /**
   * The private temporary store service.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  protected PrivateTempStore $store;

  /**
   * Constructs a PrivateStore object.
   *
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_service
   *   The private temporary store factory.
   */
  public function __construct(PrivateTempStoreFactory $temp_store_service) {
    $this->store = $temp_store_service->get('onesignal');
  }

  /**
   * Get the value for a given key.
   *
   * @param string $key
   *   The key.
   *
   * @return mixed
   *   The value.
   */
  public function getValue(string $key) {
    return $this->store->get($key) ?? '';
  }

  /**
   * Set the value for a given key.
   *
   * @param string $key
   *   The key.
   * @param mixed $value
   *   The value.
   */
  public function setValue($key, $value): void {
    try {
      $this->store->set($key, $value);
    }
    catch (TempStoreException $e) {
      // Can be ignored.
    }
  }

}
